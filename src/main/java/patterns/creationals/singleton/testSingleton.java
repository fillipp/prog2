package patterns.creationals.singleton;

public class testSingleton {

    public static void main(String[] args) {


        EagerSingleton.getInstance();
        EagerSingleton.getInstance();

        LazySingleton.getInstance();


        System.out.println(EagerSingleton.getInstance().getSth());

    }
}