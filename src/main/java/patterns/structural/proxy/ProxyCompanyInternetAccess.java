package patterns.structural.proxy;

import static patterns.structural.proxy.CompanyEmployees.isEmployee;

public class ProxyCompanyInternetAccess implements  CompanyInternetNetwork {
    CompanyInternetNetwork internetNetwork;

    @Override
    public void getAccess(String userName) {
        if (isEmployee(userName)) {
            internetNetwork = new  PrivateCompanyInternetNetwork(userName);
        } else {
            internetNetwork = new PublicCompanyInternetNetwork(userName);
        }
    }
}
