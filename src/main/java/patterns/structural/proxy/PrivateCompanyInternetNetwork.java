package patterns.structural.proxy;

public class PrivateCompanyInternetNetwork implements CompanyInternetNetwork{
    private  String username;

    public  PrivateCompanyInternetNetwork(String username){
        this.username = username;
    }
    @Override
    public void getAccess(String userName) {
        System.out.println("Generated Private network access for " + username);

    }
}