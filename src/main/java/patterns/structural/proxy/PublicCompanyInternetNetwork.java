package patterns.structural.proxy;

public class PublicCompanyInternetNetwork implements CompanyInternetNetwork {
    private  String username;

    public  PublicCompanyInternetNetwork(String username){
        this.username = username;
    }
    @Override
    public void getAccess(String userName) {
        System.out.println("Generated PUBLIC network access for " + username);

    }
}
