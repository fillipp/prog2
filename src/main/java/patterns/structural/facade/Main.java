package patterns.structural.facade;

import java.sql.Connection;

public class Main {

    public static void main(String[] args) {
        String tableName = "Employee";

        //generating mysql html report and oracle pdf report without using facade
        Connection conMySql = MySQLReportGenerator.getMySqlDBConnection();
        MySQLReportGenerator mySQLReportGenerator = new MySQLReportGenerator();

        Connection conOracle = OracleReportGenerator.getOracleDBConnection();
        OracleReportGenerator oracleReportGenerator = new OracleReportGenerator();

        //generating mysql html report and oracle pdf report using facade
        Facade.generateReport(Facade.DBType.MYSQL, Facade.ReportType.HTML, tableName);
        Facade.generateReport(Facade.DBType.ORACLE, Facade.ReportType.PDF, tableName);
    }
}
