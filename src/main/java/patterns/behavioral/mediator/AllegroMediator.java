package patterns.behavioral.mediator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class AllegroMediator implements Mediator {

    private ArrayList<Customer> customers;

    public AllegroMediator() {
        this.customers = new ArrayList<>();
    }

    @Override
    public void addBuyer(Customer customer) {
        customers.add(customer);
        System.out.println(customer.name + " was added bid list");
    }

    @Override
    public void findTopBidder() {
        int maxBid = 0;
        Customer currentWinner = null;

//        for (Customer customer : customers) {
//            if(customer.price > maxBid) {
//                maxBid = customer.price;
//                currentWinner = customer;
//            }
//}
        //lub
        currentWinner = Collections.max(customers, Comparator.comparing(customer -> customer.price));
        maxBid = currentWinner.price;

        System.out.println("Auction best price is palced by " + currentWinner.name + " with offer= " + maxBid);
    }
}
