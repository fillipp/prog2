package patterns.behavioral.templateMethod;

public class EmailNewsProvider extends NewsProvider {
    String email;

    @Override
    public boolean authorize() {
        System.out.println("Create connection");
        return false;
    }

    @Override
    public boolean endConnection() {
        System.out.println("Close connection");
        return false;
    }
}
