package patterns.behavioral.templateMethod;

public class TemplateMethodDemo {
    public static void main(String[] args) {
        NewsProvider provider = new EmailNewsProvider();
        provider.setMessage("New message");
        provider.provideNews();
    }
}
