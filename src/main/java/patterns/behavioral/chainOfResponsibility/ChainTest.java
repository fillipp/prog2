package patterns.behavioral.chainOfResponsibility;

public class ChainTest {

    public static void main(String[] args) {
        Employees employees = new Employees();
        employees.addEmployee("Jan Brzechwa", 10);
        employees.addEmployee("Edmund Niziurski", 5);
        employees.addEmployee("Bolesław Prus", 15);

        AccessCheck accessCheck = new CompanyInsiderCheck();
        accessCheck.addChainElement(new EmployeSeniorityCheck());

        XCompany company = new XCompany();
        company.setCheck(accessCheck);

        System.out.println("Not an employee");
        company.enterRoom("Jan Okoń");
        System.out.println("\nEmployee 1");
        company.enterRoom("Edmund Niziurski");
        System.out.println("\nEmployee 2");
        company.enterRoom("Bolesław Prus");
    }
}
