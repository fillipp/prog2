package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewMatcher {

    public  boolean checkNameMatches(String name) {
        String regexp = "[A-Z][a-z]*";
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

    public  boolean checkNameFind(String name) {
        String regexp = "[A-Z][a-z]*";
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(name);
        return matcher.find();
    }

    public static void main(String[] args) {
        NewMatcher newMatcher = new NewMatcher();

        System.out.println("find Jan : " + newMatcher.checkNameFind("Jan"));
        System.out.println("match Jan : " + newMatcher.checkNameMatches("Jan"));
        System.out.println("===============");
        System.out.println("find JJan : " + newMatcher.checkNameFind("JJan"));
        System.out.println("match JJan : " + newMatcher.checkNameMatches("JJan"));
        System.out.println("===============");


    }
}
