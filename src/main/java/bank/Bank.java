package bank;

import java.util.ArrayList;
import java.util.List;

public class Bank {
    private String bankName;
    private List<Client> clientBankList;

    private List<Account> accountList;

    private String customerNumber = "0";

    public Bank(String bankName) {
        this.clientBankList = new ArrayList<>();
        this.accountList = new ArrayList<>();
        this.bankName = bankName;
    }

    public void addClient(String clientName, String clientSurname, String pesel) {
        this.customerNumber = numberToString(customerNumber, 8);
        this.clientBankList.add(new Client(clientName, clientSurname, pesel, customerNumber));

    }


    public void createAccount(String pesel, AccountType accountType) {
        String customerNumber = new SearchEngine(clientBankList).searchCustomerNumber(pesel).getCustomerNumber();
        String accountNumber = customerNumber + numberToString("1",4);
        accountList.add(new Account(accountNumber, accountType));

    }

    private String numberToString(String number, int digits) {
        number = Integer.toString(Integer.parseInt(number) + 1);
        for (int i = number.length(); i < digits; i++) {
            number = "0" + number;

        }
        return number;
    }

    public List<Client> getClientBankList() {
        return clientBankList;
    }

    public List<Account> getAccountList() {
        return accountList;
    }


}
