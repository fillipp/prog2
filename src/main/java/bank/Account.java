package bank;

public class Account {
    private String accountNumber;
    private AccountType accountType;
    private int balance;

    Account(String accountNumber, AccountType accountType){
        this.accountNumber = accountNumber;
        this.accountType = accountType;
    }

    public int getBalance(){
        return 0;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    @Override
    public String toString() {
        return "Account{" +
                "Nr konta: " + accountNumber + ' ' +
                ", Typ konta: " + accountType +
                ", Saldo: " + balance +
                '}';
    }
}
