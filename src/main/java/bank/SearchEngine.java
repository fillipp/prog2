package bank;

import java.util.List;
import java.util.stream.Collectors;

public class SearchEngine<E extends Client, A extends Account> {
    private List<E> client;
    private List<A> account;


    SearchEngine(List<E> client) {
        this.client = client;
    }

    SearchEngine(List<E> client, List<A> account) {
        this.client = client;
        this.account = account;
    }


    public E searchCustomerNumber(String pesel) {

        return client.stream()
                .filter(c -> c.getPesel().equals(pesel))
                .findAny()
                .get();
    }

    public List<A> searchCustomerAccount(String pesel) {
        String customerNumber = searchCustomerNumber(pesel).getCustomerNumber();
       return  account.stream()
               .filter(e -> e.getAccountNumber().startsWith(customerNumber) == true)
               .collect(Collectors.toList());

    }
}
