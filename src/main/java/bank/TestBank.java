package bank;


public class TestBank {

    public static void main(String[] args) {

        Bank santander = new Bank("Santander");

        santander.addClient("Jan", "Kowalski", "123456789");
        santander.addClient("Jakub", "Nowak", "012345678");
        santander.addClient("Ania", "Czarny", "987654321");

        santander.createAccount("987654321", AccountType.ROR);
        santander.createAccount("987654321", AccountType.LOCATE);

        SearchEngine searchEngine = new SearchEngine(santander.getClientBankList());
        SearchEngine searchAccount = new SearchEngine(santander.getClientBankList(), santander.getAccountList());
        System.out.println(searchEngine.searchCustomerNumber("987654321").getCustomerNumber());
        System.out.println(searchAccount.searchCustomerAccount("987654321"));
        System.out.println(santander.getClientBankList());
        System.out.println(santander.getAccountList());



    }
}
