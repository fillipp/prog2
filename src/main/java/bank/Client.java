package bank;

import java.util.List;

public class Client {
    private String name;
    private String surname;
    private String pesel;
    private String customerNumber;
    private List<String> customerAccountList;

    Client(String name, String surname, String pesel, String customerNumber) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.customerNumber = customerNumber;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public List<String> getClientAccountsList(){
        return customerAccountList;
    }

    protected String getPesel() {
        return pesel;
    }

    @Override
    public String toString() {
        return  name + " " +  surname + "; " +
                "PESEL: " + pesel + "; "+
                "Nr klienta: " + customerNumber + '\n' +
                '\n';
    }
}
