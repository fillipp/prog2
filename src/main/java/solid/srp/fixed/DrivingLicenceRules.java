package solid.srp.fixed;

public class DrivingLicenceRules {
    private  static final int DRIVING_LICENCE_REQUIRED_AGE = 18;
    private  static final int DRIVING_LICENCE_REQUIRED_AGE_WITH_MENTOR = 18;


    public static boolean canGetDrivingLicence (Person person){
        //ekstraktować 18 poza klasą i wczytywać jako konfigurację
        return person.getAge() > DRIVING_LICENCE_REQUIRED_AGE;
    }
}
