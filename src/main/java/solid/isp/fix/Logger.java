package solid.isp.fix;

import java.util.Collection;

public interface Logger {
    void writeMessage(String message);
}
