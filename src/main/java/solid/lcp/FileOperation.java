package solid.lcp;

public interface FileOperation {
    byte[] read();

    void write(byte[] data);
}
