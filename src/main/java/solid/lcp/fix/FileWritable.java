package solid.lcp.fix;

public interface FileWritable {
    void write(byte[] data);
}
