package solid.lcp.fix;

public class ReadOnlyFile implements FileReadable {
    @Override
    public byte[] read() {
        //reads data
        return new byte[0];
    }

}
