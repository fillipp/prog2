package solid.lcp.fix;

public interface FileReadable {
    byte[] read();
}
