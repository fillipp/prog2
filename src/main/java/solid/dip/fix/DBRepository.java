package solid.dip.fix;

public class DBRepository implements Repository {

    @Override
    public void deleteTask(int taskId) {
    }

    @Override
    public void saveTask(String task) {
    }
}
