package solid.dip.fix;

public class RunTest {
    public static void main(String[] args) {
        Repository repository = new FileRepository();
        TaskService taskService= new TaskService(repository);

        taskService.addTask("task1");

        //=====================

        repository = new DBRepository();

        TaskService taskService1  = new TaskService(repository);

        taskService1.removeTask(1);

    }
}
