package solid.dip.fix;

public interface Repository {
    void deleteTask(int taskId);

    void saveTask(String task);
}
