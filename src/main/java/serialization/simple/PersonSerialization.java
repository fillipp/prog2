package serialization.simple;

import serialization.Person;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class PersonSerialization {

    public static void main(String[] args) {
        Person person1 = new Person("Jan", "Nowak", 33, "M",2, "PL");

        try {
            //tworzymy strumień wyjsciowy i przekazujemy objekt do pliku
            FileOutputStream fOut = new FileOutputStream("PersonSerialized.data");
            ObjectOutputStream outputStream = new ObjectOutputStream(fOut);
            outputStream.writeObject(person1);
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
