package serialization.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import serialization.Person;

import java.io.File;
import java.io.IOException;

public class PersonJsonUtil {

    public static void mapToPersonToJson(Person person) {
        //klasa do odwzorowania wskazanych objektow na strumien danych json
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = null;

        try {
            jsonString = objectMapper.writeValueAsString(person);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        System.out.println(jsonString);
    }

    public static void savePersonToJsonFile(Person person) {
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new File("personJson.json");

        try {
            //zapisujemy do pliku objekt person przeksztalcony do jsona
            objectMapper.writeValue(file, person);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Person readPersonFromJsonFile(String fileName) {
        ObjectMapper objectMapper = new ObjectMapper();
        Person person =null;
        File file = new File(fileName);

        try {
            person = objectMapper.readValue(file, Person.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return person;
    }
}
