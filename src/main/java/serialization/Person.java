package serialization;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name= "person")

//zeby serializowac klase trzeba implementowac serializable
public class Person implements Serializable {
    private String firstName;
    private String lastName;
    private transient int age; // transient oznacza ze to pole ma sie nieserializować
    private String gender;
    private int noOfChildren;
    @JsonIgnore

    private String nationality;

    /**
     * konstruktor bezargumentowy na potrzeby JAXB
     */

    public Person() {
    }


    public Person(String firstName, String lastName, int age, String gender, int noOfChildren, String nationality) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
        this.noOfChildren = noOfChildren;
        this.nationality = nationality;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getNoOfChildren() {
        return noOfChildren;
    }

    public void setNoOfChildren(int noOfChildren) {
        this.noOfChildren = noOfChildren;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", noOfChildren=" + noOfChildren +
                ", nationality='" + nationality + '\'' +
                '}';
    }
}
