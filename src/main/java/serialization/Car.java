package serialization;

public class Car {
    private String name;
    private String color;
    private int year;
    private String type; //Sedan, hatchback, ...

    public Car(String name, String color, int year, String type) {
        this.name = name;
        this.color = color;
        this.year = year;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
