package serialization.jaxb.xml;

import serialization.Person;
import serialization.Persons;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class XmlConverterTest {

    public static void main(String[] args) {
        Person person = new Person("Anna", "Nowak", 22, "K",1,"PL");
        Person person2 = new Person("MAria", "Paluch", 12, "K",3,"PL");
        Person person3 = new Person("Edward", "Gierek", 88, "M",1,"PL");
        Person person4 = new Person("Anna", "Mucha", 28, "K",1,"PL");

        List<Person> personList = new ArrayList<>();
        personList.add(person);
        personList.add(person2);
        personList.add(person3);
        personList.add(person4);
        Persons people = new Persons();
        people.setPeople(personList);
        System.out.println("===========People list XML===========");

        //Person do xml na ekran
        PeopleXmlUtil.peopleToXmlString(people);

        //Person do xml do pliku
        PeopleXmlUtil.peopleToXMLFile(people);
        System.out.println("======Print XML========");
        Optional<Persons> optionalPersons = PeopleXmlUtil.xmlFileToPeople("peopleToXml.xml");
        if (optionalPersons.isPresent()) {
            System.out.println(optionalPersons.get());
            optionalPersons.get().getPeople().forEach(System.out::println);
        } else {
            System.out.println("nobody");
        }
    }
}
