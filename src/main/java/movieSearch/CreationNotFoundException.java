package movieSearch;

public class CreationNotFoundException extends Exception {
    public CreationNotFoundException() {
        super("Movie not found");
    }
}
