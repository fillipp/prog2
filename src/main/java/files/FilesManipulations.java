package files;


import java.io.IOException;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilesManipulations {

    public static void readFile(String path) throws IOException {

        Stream<String> inputValues = Files.lines(Paths.get(path));

        List<String> stringList = inputValues.collect(Collectors.toList());

        int i = 0;
        for (String line : stringList) {

            System.out.println(i++ + ": " + line);

        }
    }

    public static void writeText(String path, String text) throws IOException {
        Files.write(Paths.get(path), text.getBytes());

    }

    public static void addTextToFile(String path, String text) throws IOException {
        Files.write(Paths.get(path), text.getBytes(), StandardOpenOption.APPEND);
    }

    public static List<Path> readListFilteredFiles(String path, String extension) throws IOException {
        List<Path> collect = Files.list(Paths.get(path))
                .filter(f -> f.getFileName().toString().endsWith("." + extension) == true)
                .collect(Collectors.toList());

        collect.forEach(System.out::println);
        return collect;
    }

    public static void readSampleFiles(String folderPath, String extension) throws IOException {
        System.out.println("Lista plików w folderze z rozszerzeniem *." + extension + ":");
        List<Path> paths = readListFilteredFiles(folderPath, extension);

        System.out.println();
        paths.forEach(p -> {
            try {
                System.out.println("Sample(" + p.getFileName() + "):\n" + Files.lines(p).collect(Collectors.toList()).get(0) + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });


    }

}
